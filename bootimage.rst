===================
Boot Image Building
===================

Mu provides an interface to build "boot images". A boot image is a file that
contains a Mu IR bundle, a serialised Mu memory (only global cells and heap
objects), and implementation-specific data (such as the micro VM itself, the Mu
function as the entry point, external linkages, and statically-linked native
libraries).

Motivation
==========

The purpose for this mechanism is to allow fast VM initialisation.

Typical language runtimes (such as the ``java`` executable for JVM and the
``pypy`` executable for PyPy), when implemented on Mu, need to have many things
initialised when they start:

- **pre-loaded bundles**: These include the built-in data types and the
  essential parts of the standard library of the language. For dynamic
  languages, this part can be huge, because even the simplest operations (such
  as "adding") must be implemented as complex routines. For some implementations
  (such as PyPy), it also includes the interpreter or the metacircular client.

- **pre-allocated heap objects and pre-initialised memory**: These are the
  essential built-in objects of the language in question. For some Java
  implementations, the instance of ``java.lang.Class<Object>``, the default
  ``java.lang.ClassLoader``, the ``System.in/out`` IO objects, and their TIBs
  can be pre-initialised.

- **external linkages**: Realistic language clients need to call system
  functions, such as POSIX functions provided by libc as C functions. For
  practical reasons, some parts of the language runtime may have to be written
  in C rather than implemented as Mu functions. From Mu's point of view, C
  functions are function pointers (``ufuncptr``), which are word-sized integers.
  But the pointer values are not resolved until load time. Traditional C
  programs address external functions symbolically (using function names, such
  as ``open``, ``read``, ``write``), but leave *unresolved symbols* and
  *relocation entries* in the executable file images (ELF, MachO, PE, etc.). At
  load time, the dynamic loader loads different ``.so``, ``.dylib`` or ``.dll``
  files into different regions of the address space of the process, and fixes
  relocation entries.

- **inter-references by untraced pointers**: In addition to calling foreign
  functions, language runtimes may also need to create global variables of
  struct types which contain pointers to global variables or functions defined
  in native modules. Symbol resolution is not a problem for JIT (``dlsym`` will
  work), but when creating boot images, the actual addresses of these fields can
  only be resolved at load time.

  Take the following C program as example::

    struct Foo {
        int bar;
        int (*some_func_ptr)(char*);
    };

    struct Foo my_foo = { 42, puts };

  At load time, the ``my_foo.some_func_ptr`` field is initialised to the
  address of the ``puts`` function in libc. But the address is different each
  time the program runs. So relocation entries need to be placed at the
  ``my_foo.some_func_ptr`` field so that the dynamic linker can fill in its
  value at load time.
  
  There is a pure client-side solution: (1) record such fields at compile time
  as a list of IRefs in the boot image, then (2) resolve the symbols manually
  (using ``dlsym``, the ``EXTERN`` Mu constants, or calculating the address of
  Mu object fields from IRefs) at load time, and (3) assign them to the fields.
  But this approach cannot make use of the system dynamic linker.

A good language runtime must start up fast. Thus it is not practical to use the
JIT compiler to compile all of the standard library functions at start-up time,
but the boot image builder should AoT compile the initial Mu IR bundle into
machine code, and simply memory-map the machine code into the memory. The same
is true for run-time heap objects: the heap should be serialised and bulk-copied
into the heap. It should also make use of system utilities (such as the dynamic
linker/loader) to load external libraries and resolve external symbols, thus
metadata should be provided.

Mu IR Changes
=============

The IR can define `external constants <ir.rst#external-constructor>`__. They are
resolved in an implementation-specific manner when the bundle is loaded. The API
function `new_const_extern <irbuilder.rst#new-const-extern>`__ creates external
constants programmatically.

Global cells are permanently pinned. The new ``get_addr`` `API
<api.rst#get-addr>`__/`CommInst <common-insts.rst#get-addr>`__ gets the address
of already pinned memory locations, including global cells.

Mu API Changes
==============

The `MuCtx.make_boot_image <api.rst#make-boot-image>`__ will create the boot
image. Basically, it specifies what's in the boot image by a white-list of
top-level definitions, and saves all things reachable from them. It also lets
the client specify a list of memory locations in global cells where symbols will
be placed, and a list of memory locations in global cells and heap objects which
will be filled with the address of other symbols at load time.

It also lets the client specify the initial state of a primordial thread which
can be the entry point of pure Mu IR programs.

.. vim: tw=80
