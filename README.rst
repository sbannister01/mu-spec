================
Mu Specification
================

This document aims to provide a detailed description of Mu, a micro virtual
machine, including its architecture, instruction set and type system.

Main specification:

- `Overview <overview.rst>`__
- `Intermediate Representation (IR) <ir.rst>`__
- `Intermediate Representation Binary Form (deprecated) <ir-binary.rst>`__
- `Type System <type-system.rst>`__
- `Instruction Set <instruction-set.rst>`__
- `Common Instructions <common-insts.rst>`__
- `Client Interface (a.k.a. The API) <api.rst>`__
- `Call-based IR Building API <irbuilder.rst>`__
- `Threads and Stacks <threads-stacks.rst>`__
- `Memory and Garbage Collection <memory.rst>`__
- `Memory Model <memory-model.rst>`__
- `(Unsafe) Native Interface <native-interface.rst>`__
- `Heap Allocation and Initialisation Language (HAIL) <hail.rst>`__
- `Boot Image Building <bootimage.rst>`__
- `Portability and Implementation Advices <portability.rst>`__

Platform-specific parts: These extends the main specification. The main
specification considers these parts as implementation-specific.

- `AMD64 Unix Native Interface <native-interface-x64-unix.rst>`__

Frequently asked questions:

- `FAQ <faq.rst>`__

.. vim: tw=80
