"""
Read muapi.h and write common instruction definitions in reStructuredText.

Usage: python3 scripts/muapi-irbuilder-to-comminsts.py < muapi.h

Then paste into common-insts.rst
"""

import muapiparser
import sys

from muspecinjectablefiles import injectable_files, muapi_h_path

start_id_comminst = 0x300
start_id_constant = 0x400

_type_map = {
        "void"            : "",
        "MuID"            : "int<32>",
        "MuName"          : "iref<int<8>>",
        "MuCString"       : "iref<int<8>>",
        "MuBool"          : "int<32>",
        "MuWPID"          : "int<32>",
        "MuArraySize"     : "int<64>",
        "MuBinOpStatus"   : "int<32>",
        "MuBinOptr"       : "int<32>",
        "MuCmpOptr"       : "int<32>",
        "MuConvOptr"      : "int<32>",
        "MuDestKind"      : "int<32>",
        "MuMemOrd"        : "int<32>",
        "MuAtomicRMWOptr" : "int<32>",
        "MuCallConv"      : "int<32>",
        "MuCommInst"      : "int<32>",
        "MuFlag"          : "int<32>",
        "int"             : "int<32>",
        "long"            : "int<64>",
        "int8_t"          : "int<8>",
        "uint8_t"         : "int<8>",
        "int16_t"         : "int<16>",
        "uint16_t"        : "int<16>",
        "int32_t"         : "int<32>",
        "uint32_t"        : "int<32>",
        "int64_t"         : "int<64>",
        "uint64_t"        : "int<64>",
        "intptr_t"        : "int<64>",
        "uintptr_t"       : "int<64>",
        "float"           : "float",
        "double"          : "double",
        }

def to_mu_ty(cty):
    if cty == 'MuIRBuilder*':
        return 'irbuilderref'
    elif cty.endswith("*"):
        return "iref<{}>".format(to_mu_ty(cty[:-1]))
    elif cty.startswith("Mu") and (cty.endswith("Node") or cty.endswith("Clause")):
        return "int<32>"
    elif cty in _type_map:
        return _type_map[cty]
    else:
        raise Exception("I don't know how to translate: {}".format(cty))

def get_comminst_defs(ast):
    muctx_methods = [s["methods"] for s in ast["structs"]
            if s["name"] == "MuIRBuilder"][0]

    next_id = start_id_comminst

    lines = ["", "::", ""]  # start reStructuredText block

    for i in range(len(muctx_methods)):
        meth = muctx_methods[i]
        name = meth["name"]
        params = meth["params"]
        ret_ty = meth["ret_ty"]

        mu_params = []

        for param in params:
            pn = param["name"]
            pt = param["type"]
            mpn = "%" + pn
            mpt = to_mu_ty(pt)

            mu_params.append("{}: {}".format(mpn, mpt))

        mu_params_joined = ", ".join(mu_params)
        mu_ret_ty = to_mu_ty(ret_ty)

        lines.append("    [0x{:x}]@uvm.irbuilder.{} ({}) -> ({})".format(
            next_id, name, mu_params_joined, mu_ret_ty))

        next_id += 1

    lines.append("")
    lines.append("")

    return "\n".join(lines)

_enums = [
    "MuBinOptr",
    "MuCmpOptr",
    "MuConvOptr",
    "MuMemOrd",
    "MuAtomicRMWOptr",
    "MuCallConv",
    "MuCommInst",
    ]

def main():
    with open(muapi_h_path) as f:
        txt = f.read()

    ast = muapiparser.parse_muapi(txt)

    outtext = get_comminst_defs(ast)

    injectable_files["common-insts.rst"].inject_many({
        "IRBUILDER_COMMINSTS": outtext,
        })

if __name__=='__main__':
    main()
