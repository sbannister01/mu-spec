import injecttools
import os.path

_my_dir = os.path.dirname(__file__)
_mu_spec_root = os.path.join(_my_dir, "..")

def _make_injectable_file_set(m):
    m2 = {os.path.join(_mu_spec_root, k): v for k,v in m.items()}
    return InjectableFileSet(m2)

muapi_h_path = os.path.join(_mu_spec_root, "muapi.h")
common_insts_path = os.path.join(_mu_spec_root, "common-insts.rst")

injectable_files = injecttools.make_injectable_file_set(_mu_spec_root, [
    ("common-insts.rst", "common-insts.rst",
        ["IRBUILDER_COMMINSTS"]),
    ("muapi.h", "muapi.h",
        ["COMMINSTS"]),
    ])
